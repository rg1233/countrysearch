#FROM nginx:1.20.1-alpine@sha256:bac218df22fef66a173cfa65d0dfa0acca80a3a39df41665be742596977b6c7c
FROM alpine
RUN apk add --no-cache python3 py3-pip uwsgi-python3
EXPOSE 3031
COPY /app/requirements.txt /usr/requirements.txt
RUN pip install -r  /usr/requirements.txt
COPY ./app ./app
WORKDIR ./app
CMD ["uwsgi", "--socket", "0.0.0.0:3031",\
              "--uid", "uwsgi", \
              "--plugins", "python3", \
              "--protocol", "uwsgi", \
              "--wsgi", "app:app"]