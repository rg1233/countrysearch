from typing import List
import requests
import json

class CountryAPI:

    @classmethod
    def search_by_name(cls, name, fields=None):
        return cls._query_endpoint("/name", name, fields)        
    @classmethod
    def search_by_full_name(cls, name, fields=None):
        filters = {"fullText": "true"}
        return cls._query_endpoint("/name", name, fields, filters)        

    @classmethod
    def search_by_country_code(cls, code, filters=None):
        if len(code) > 3 or len(code) < 2: raise Exception("Invalid input")
        return cls._query_endpoint("/alpha", code, filters)

    @classmethod
    def _query_endpoint(cls, resource, value, fields=None, filters={}):
        if fields is not None:            
            if "alpha2Code" not in fields:
                fields.add("alpha2Code")             
            filters["fields"]=",".join(fields)
        querySeparator = "?"
        queryFilter = []
        for filter in filters:
            queryFilter.append(querySeparator + filter + "=" + str(filters[filter]))
            querySeparator="&"
        filterStr = "".join(queryFilter)        

        url = "{}{}/{}{}".format("https://restcountries.eu/rest/v1",
        resource, value, filterStr)

        response = requests.get(url)
        if response.status_code == 404:
            return []
        elif response.status_code == 200:
            results = []
            parsed = json.loads(response.text)
            if type(parsed)==list:
                for country in parsed:
                    if country:
                        results.append(Country(country))
            else:
                results.append(Country(parsed))
        return results

class Country:
    def __str__(self):
        return "{}".format(self.name)

    def __init__(self, country_data):
        self.top_level_domain = country_data.get("topLevelDomain")
        self.alpha2_code = country_data.get("alpha2Code")
        self.alpha3_code = country_data.get("alpha3Code")
        self.currencies = country_data.get("currencies")
        self.capital = country_data.get("capital")
        self.calling_codes = country_data.get("callingCodes")
        self.alt_spellings = country_data.get("altSpellings")
        self.relevance = country_data.get("relevance")
        self.region = country_data.get("region")
        self.subregion = country_data.get("subregion")
        self.translations = country_data.get("translations")
        self.population = country_data.get("population")
        self.latlng = country_data.get("latlng")
        self.demonym = country_data.get("demonym")
        self.area = country_data.get("area")
        self.gini = country_data.get("gini")
        self.timezones = country_data.get("timezones")
        self.borders = country_data.get("borders")
        self.native_name = country_data.get("nativeName")
        self.name = country_data.get("name")
        self.numeric_code = country_data.get("numericCode")
        self.languages = country_data.get("languages")
        self.flag = country_data.get("flag")
        self.regional_blocs = country_data.get("regionalBlocs")
        self.cioc = country_data.get("cioc")

    def __eq__(self, other):
        assert isinstance(other, Country)
        return self.alpha3_code == other.alpha3_code

    def __lt__(self, other):
        assert isinstance(other, Country)
        return self.population < other.population

    def __hash__(self):
        return int(self.alpha3_code, base=36)

    def __str__(self):
        return "<{} | {}>".format(self.name, self.alpha3_code)

    def __repr__(self):
        return "<{} | {}>".format(self.name, self.alpha3_code)

