from flask import Flask
import flask
from countryAPI import CountryAPI as countryAPI, Country
app = Flask(__name__)

FILTER_FIELDS = ["name","alpha2Code","alpha3Code","flag","regionalBlocs","population","languages","numericCode"] #Note - numericCode is used by the library for comparison

@app.route("/API/searchAll/<searchString>")
def searchAllCategories(searchString: str) -> str:            
    CountrySet = set[Country]
    currentResults: CountrySet = set(countryAPI.search_by_name(searchString, FILTER_FIELDS))
    if len(searchString) <= 3 and len(searchString) > 1: currentResults = currentResults.union(countryAPI.search_by_country_code(searchString, FILTER_FIELDS))       
    JSONDict = JSONifyCountrySet(currentResults)
    return JSONDict

@app.route("/API/exactNameSearch/<searchString>")
def searchExactName(searchString: str) -> str:
    return JSONifyCountrySet(countryAPI.search_by_full_name(searchString, FILTER_FIELDS))

def JSONifyCountrySet(currentResults):
    JSONListOfDicts = []
    for country in sorted(list(currentResults)):
        if country is None: continue
        JSONListOfDicts.append({
            "name": country.name,
            "alpha2Code": country.alpha2_code,
            "alpha3Code": country.alpha3_code,
            "flag": country.flag,
            "region": country.region,
            "subregion": country.subregion,
            "population": country.population,
            "languages": country.languages
        })
        
    return flask.jsonify(JSONListOfDicts)


    

if __name__ == '__main__':    
    app.run()